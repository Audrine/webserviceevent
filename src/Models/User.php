<?php 

namespace WebServiceEvent\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
	
	protected $table = 'users';

	protected $fillable = [
		'nom',
		'prenom',
		'pseudo',
		'email',
		'password',
	];


	public function setPassword($password)
	{

		$this->update([

			'password' => password_hash($password, PASSWORD_DEFAULT)

			]);
		
	}

}