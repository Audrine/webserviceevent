<?php 

namespace WebServiceEvent\Models;

use Illuminate\Database\Eloquent\Model;

class Soirees extends Model
{
	
	protected $table = 'soirees';

	protected $fillable = [
		'id_users',
		'titre',
		'date',
		'heure',
		'lieu',
		'description',
		'image',
		
	];


}