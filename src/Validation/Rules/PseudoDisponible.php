<?php

namespace WebServiceEvent\Validation\Rules;


use WebServiceEvent\Models\User;

use Respect\Validation\Rules\AbstractRule;


class PseudoDisponible extends AbstractRule

{

	public function validate($input)

	{

		return User::where('pseudo', $input)->count() === 0;
		
	}

}