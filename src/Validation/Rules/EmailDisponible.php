<?php

namespace WebServiceEvent\Validation\Rules;


use WebServiceEvent\Models\User;

use Respect\Validation\Rules\AbstractRule;


class EmailDisponible extends AbstractRule

{

	public function validate($input)

	{

		return User::where('email', $input)->count() === 0;
		
	}

}