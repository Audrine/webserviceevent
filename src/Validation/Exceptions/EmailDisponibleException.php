<?php

namespace WebServiceEvent\Validation\Exceptions;


use Respect\Validation\Exceptions\ValidationException;



class EmailDisponibleException extends ValidationException

{

	public static $defaultTemplates = [

		self::MODE_DEFAULT => [

			self::STANDARD => 'Cet email est déja utilisé.',

		],

	];

}