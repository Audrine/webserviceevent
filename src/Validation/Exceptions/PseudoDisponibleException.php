<?php

namespace WebServiceEvent\Validation\Exceptions;


use Respect\Validation\Exceptions\ValidationException;



class PseudoDisponibleException extends ValidationException

{

	public static $defaultTemplates = [

		self::MODE_DEFAULT => [

			self::STANDARD => 'Ce pseudo est déja utilisé.',

		],

	];

}