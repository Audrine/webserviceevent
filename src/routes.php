<?php
// Routes

use WebServiceEvent\Middleware\AuthMiddleware;

use WebServiceEvent\Middleware\GuestMiddleware;


$app->get('/', 'AccueilController:index')->setName('home');

$app->group('', function () {

	$this->get('/auth/inscription', 'AuthController:getInscription')->setName('auth.inscription');

	$this->post('/auth/inscription', 'AuthController:postInscription');

	$this->get('/auth/connection', 'AuthController:getConnection')->setName('auth.connection');

	$this->post('/auth/connection', 'AuthController:postConnection');

})->add(new GuestMiddleware($container));


$app->get('/gest/soiree', 'ControllerSoiree:getRechercheSoiree')->setName('gest.soiree');

$app->get('/gest/soiree/{id}', 'ControllerSoiree:getAfficheSoiree')->setName('gest.Affichesoiree');

$app->get('/gest/modifier', 'ControllerSoiree:getUpdateSoiree')->setName('gest.modifier');

$app->post('/gest/modifier', 'ControllerSoiree:postUpdateSoiree');

$app->get('/gest/delete', 'ControllerSoiree:postDeleteSoiree');

$app->get('/about/contact', 'ContactController:getContact')->setName('about.contact');



$app->group('', function () {

	$this->get('/ajout/ajoutSoiree', 'ControllerSoiree:getAjoutSoiree')->setName('ajout.ajoutSoiree');

	$this->post('/ajout/ajoutSoiree', 'ControllerSoiree:postAjoutSoiree');

	$this->get('/auth/deconnection', 'AuthController:getDeconnection')->setName('auth.deconnection');

	$this->get('/auth/supression', 'AuthController:getSupression')->setName('auth.supression');

	$this->get('/auth/password/change', 'PasswordController:getChangePassword')->setName('auth.password.change');

	$this->post('/auth/password/change', 'PasswordController:postChangePassword');

})->add(new AuthMiddleware($container));





