<?php

namespace WebServiceEvent\Middleware;


class GuestMiddleware extends Middleware
{


	public function __invoke($request, $response, $next)
	{
 
		if ($this->container->daouser->check()) {

			return $response->withRedirect($this->container->router->pathfor('home'));
			
		}

 			

		$response = $next($request, $response);

		return $response;

	}
}