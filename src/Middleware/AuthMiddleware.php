<?php

namespace WebServiceEvent\Middleware;

//class pour autoriser l'accès a certaine fonction que pour les users

class AuthMiddleware extends Middleware
{


	public function __invoke($request, $response, $next)
	{
 
 		//vérifier si l'utilisateur n'est pas connecté
 		if (!$this->container->daouser->check()) {

 			//flash
 			$this->container->flash->addMessage('error', 'Connectez-vous pour utiliser cette fonction.');

 			//redirect
 			return $response->withRedirect($this->container->router->pathfor('auth.connection'));

 		}

 			
		$response = $next($request, $response);

		return $response;

	}
}