<?php

namespace WebServiceEvent\Controllers\ContactController;

use WebServiceEvent\Controllers\Controller;
use Slim\Views\Twig as View;

class ContactController extends Controller
{
		public function getContact($request, $response)
	{		
		return $this->view->render($response, 'about/contact.twig');
	}
}