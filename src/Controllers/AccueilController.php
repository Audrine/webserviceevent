<?php

namespace WebServiceEvent\Controllers;

use WebServiceEvent\Models\User;
use WebServiceEvent\Models\Soirees;
use Slim\Views\Twig as View;

class AccueilController extends Controller
{
		public function index($request, $response)
		{

			return $this->view->render($response, 'home.twig');
		}	
}