<?php

namespace WebServiceEvent\Controllers\Authentication;

use WebServiceEvent\Models\User;
use WebServiceEvent\Controllers\Controller;
use Respect\Validation\Validator as v;



class PasswordController extends Controller 
{


	public function getChangePassword($request, $response)
	{


		return $this->view->render($response, 'auth/password/change.twig');
	}

	public function postChangePassword($request, $response)
	{

		$validation = $this->validator->validate($request, [

			'password_old' => v::noWhitespace()->notEmpty()->matchesPassword($this->daouser->user()->password),
			'password' => v::noWhitespace()->notEmpty(),

			]);

		if ($validation->failed()) {

			return $response->withRedirect($this->router->pathFor('auth.password.change'));

		}

		$this->daouser->user()->setPassword($request->getParam('password'));

		//ingo message
		$this->flash->addMessage('info', 'Votre mot de passe à été changé.' );

		//redirect
		return $response->withRedirect($this->router->pathFor('home'));

	}


}