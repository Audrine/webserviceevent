<?php

namespace WebServiceEvent\Controllers\Authentication;

use WebServiceEvent\Models\User;
use WebServiceEvent\Controllers\Controller;
use Respect\Validation\Validator as v;

class AuthController extends Controller 
{

		public function getSupression($request, $response)
		{

			$this->daouser->supression();

			$this->flash->addMessage('info', 'Votre compte à été suprimé avec succès!');
			
			return $response->withRedirect($this->router->pathFor('home'));
			
		}


		public function getDeconnection($request, $response)
		{

			$this->daouser->deconnection();
			
			return $response->withRedirect($this->router->pathFor('home'));
			
		}

		public function getConnection($request, $response)
		{

			return $this->view->render($response, 'auth/connection.twig');
		}

		public function postConnection($request, $response)
		{

			$auth = $this->daouser->attempt(

				$request->getParam('email'),
				$request->getParam('password')
				

			);

			if (!$auth) {

				$this->flash->addMessage('error', 'Connexion échouée. Avez-vous un compte? Si non, inscrivez-vous!');

				return $response->withRedirect($this->router->pathFor('auth.connection'));


			}

			return $response->withRedirect($this->router->pathFor('home'));


		}
		





		public function getInscription($request, $response)
		{

			return $this->view->render($response, 'auth/inscription.twig');
		}

		public function postInscription($request, $response)
		{
			

			$validation = $this->validator->validate($request, [

					'pseudo' => v::noWhitespace()->notEmpty()->pseudoDisponible(),
					'nom' => v::noWhitespace()->notEmpty()->alpha(),
					'prenom' => v::noWhitespace()->notEmpty()->alpha(),
					'email' => v::noWhitespace()->notEmpty()->email()->emailDisponible(),
					'password' => v::noWhitespace()->notEmpty(),

				]);

			if ($validation->failed()) {

				return $response->withRedirect($this->router->pathFor('auth.inscription'));
			}


			$user = User::create([
				'pseudo' => $request->getParam('pseudo'),
				'nom' => $request->getParam('nom'),
				'prenom' => $request->getParam('prenom'),
				'email' => $request->getParam('email'),
				'password' => password_hash($request->getParam('password'), PASSWORD_DEFAULT, ['cost' => 10]),

				]);

			$this->flash->addMessage('info', 'Vous avez bien été inscrit!');

			$this->daouser->attempt($user->email, $request->getParam('password'));




			return $response->withRedirect($this->router->pathFor('home'));
		}


}