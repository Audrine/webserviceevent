<?php

namespace WebServiceEvent\Controllers\ControllerSoiree;

use WebServiceEvent\Controllers\Controller;
use WebServiceEvent\Models\Soirees;
use Respect\Validation\Validator as v;


function get_random_string($length)
{
	$charset='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    // start with an empty random string
    $random_string = "";

    // count the number of chars in the valid chars string so we know how many choices we have
    $num_valid_chars = strlen($charset);

    // repeat the steps until we've created a string of the right length
    for ($i = 0; $i < $length; $i++)
    {
        // pick a random number from 1 up to the number of valid chars
        $random_pick = mt_rand(1, $num_valid_chars);

        // take the random character out of the string of valid chars
        // subtract 1 from $random_pick because strings are indexed starting at 0, and we started picking at 1
        $random_char = $charset[$random_pick-1];

        // add the randomly-chosen char onto the end of our string so far
        $random_string .= $random_char;
    }

    // return our finished random string
    return $random_string;
}


class ControllerSoiree extends Controller 
{
	//################################################################################
	// Controlleurs d'ajout de soirées 
	//################################################################################

	// GET 
	public function getAjoutSoiree($request, $response)
	{		
		return $this->view->render($response, 'ajout/ajoutSoiree.twig');
	}


	//POST
	public function postAjoutSoiree($request, $response)
	{
		
		$validation = $this->validator->validate($request, [

				'titre' => v::notEmpty(),
				'date' => v::notEmpty(),
				'heure' => v::notEmpty(),
				'lieu' => v::notEmpty(),
				'description' => v::notEmpty(),
				// 'image' => v::uploaded()

			]);
		
		$files = $request->getUploadedFiles();
		if ($validation->failed()) {

			return $response->withRedirect($this->router->pathFor('ajout.ajoutSoiree'));

		}
		//ajout de l'image erreurs?
		if (empty($files['image'])) {
    		$this->flash->addMessage('error', 'Veuillez insérer une image jpg ou png');
			return $response->withRedirect($this->router->pathFor('ajout.ajoutSoiree'));
		}
		$fileimage = $files['image'];
		if ($fileimage->getError() === UPLOAD_ERR_OK) {
			$image = get_random_string(7); 
		    $fileimage->moveTo("./images/$image"); 
		} 
		else{
			$this->flash->addMessage('error', 'Votre image doit être au minimum à 1 Mo');
			return $response->withRedirect($this->router->pathFor('ajout.ajoutSoiree'));
		}
		$info_soiree = array();
		// Recuperation des donnees POST
		foreach ($request->getParams() as $key => $value) {
			if($key != 'csrf_name' and $key != 'csrf_value'){
				$info_soiree[$key] = $value;
			}
		}
		$info_soiree['image'] = $image;
		$info_soiree['id_users'] = $_SESSION['user'] ;
		// Insertion des donnees en base
		$this->daosoiree->insert($info_soiree);
		$this->flash->addMessage('info', 'Vous avez bien ajouté un évènement!');
		return $response->withRedirect($this->router->pathFor('gest.soiree'));
	}






	//################################################################################
	// Controlleurs affichage liste soirée
	//################################################################################

	//GET 
	public function getRechercheSoiree($request, $response)
	{
		
		//rechercher une soirée

		$titre = $request->getParam('titre');
		$lieu  = $request->getParam('lieu'); 

		if(!empty($titre) or !empty($lieu))
		{
			$soirees = $this->daosoiree->rechercheSoireeParTitreLieuDate($titre, $lieu);
		}
		else{
			$soirees = $this->daosoiree->getAll(); 
		}

		//retourner la page avec les soirées
		return $this->view->render($response, 'gest/soiree.twig', array(
			'soirees' => $soirees,
			));
	}






	//################################################################################
	// Controlleurs affichage d'une soirée 
	//################################################################################
	public function getAfficheSoiree($request, $response, $args)
	{
		
		//voir si la soirée appartient au user connecté => affichage des boutons
		$id = $args['id'];
		$soiree = $this->daosoiree->getSoiree($id);
		
		if(!isset($_SESSION['user']) or $soiree->id_users != $_SESSION['user']){
			$afficher_bouton = False;
		}
		else{
			$afficher_bouton = True; 
		}

		
		//retourner la soirée avec ou sans boutons
		return $this->view->render($response, 'gest/afficheSoiree.twig', array(
			'soiree' => $soiree,
			'afficher_bouton' => $afficher_bouton
			));
	}







	//################################################################################
	// Controlleurs affichage d'une soirée 
	//################################################################################

	//GET
	public function getUpdateSoiree($request, $response)
	{
		$soiree = $this->db->table('soirees')->find($request->getParam('id_soiree'));
		return $this->view->render($response, 'gest/modifier.twig', array(
			'soiree' => $soiree,
			));
	}


	//POST
	public function postUpdateSoiree($request, $response)
	{
		// var_dump($request->params());
		$new_soiree = array();
		foreach ($request->getParams() as $key => $value) {
			if($key == 'id_soiree'){
				$id_soiree = $value;
			}
			elseif($key != 'csrf_name' and $key != 'csrf_value'){
				$new_soiree[$key] = $value;
			}
		}
		$this->daosoiree->update($id_soiree, $new_soiree);

		$this->flash->addMessage('info', 'Votre soirée à bien été modifier!');

		return $response->withRedirect($this->router->pathFor('gest.Affichesoiree', ['id' => $id_soiree]));
	}







	//################################################################################
	// Controlleurs suppression soiree 
	//################################################################################
	public function postDeleteSoiree($request, $response)
	{
		$id_soiree = $request->getParam('id_soiree');

		$this->daosoiree->delete($id_soiree);

		$this->flash->addMessage('info', 'Votre soirée à bien été supprimer!');

		return $response->withRedirect($this->router->pathFor('gest.soiree'));

	}

}