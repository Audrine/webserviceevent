<?php


namespace WebServiceEvent\Dao;



use WebServiceEvent\Models\Soirees;


class DAOSoiree
{

	public function update($id_soiree, $new_soiree){
		Soirees::where('id', $id_soiree)
		         ->update($new_soiree);
	}
	public function delete($id_soiree){
		Soirees::destroy($id_soiree);
	}
	public function insert($info_soiree){
		Soirees::create($info_soiree);
	}

	public function rechercheSoireeParTitreLieuDate($titre, $lieu){

		if(empty($lieu)){
			return Soirees::where('titre' , 'LIKE', "%$titre%")
						->get();
		}
		elseif (empty($titre)) {
			return Soirees::where('lieu' , 'LIKE', "%$lieu%")
						->get();
		}
		else
			return Soirees::where('titre' , 'LIKE', "%$titre%")
						->orWhere('lieu', 'LIKE', "%$lieu%")
						->get();

		
	}

	public function getAll(){
		return Soirees::all(); 
	}

	public function getSoiree($id){
		return Soirees::find($id);
	}
}

