<?php


namespace WebServiceEvent\Dao;



use WebServiceEvent\Models\User;

class DAOUser

{

	public function user()

	{
		if (!empty($_SESSION['user'])) {
		return User::find($_SESSION['user']);

	}


	}

	public function check()
	{
		return isset($_SESSION['user']);
	}

	public function attempt($email, $password)

	{

		$user = User::where('email', $email)->first();

		if (!$user) {

			return false;
		}


		if (password_verify($password, $user->password)) {

			$_SESSION['user'] = $user->id;

			return true;
		}

		return false;
		
	}

	public function deconnection()

	{

		unset($_SESSION['user']);
	}

	public function supression()

	{

		User::destroy($_SESSION['user']);
		unset($_SESSION['user']);
		
		
	}

	public function creatUser($user_info)
	{
		$user = User::create($user_info);
	}

}

