<?php

use Respect\Validation\Validator as v;

session_start();

require __DIR__ . '/../vendor/autoload.php';



$user = new \WebServiceEvent\Models\User;

$soirees = new \WebServiceEvent\Models\Soirees;

//connection bdd
$app = new \Slim\App([
	'settings' => [
        'displayErrorDetails' => true, // set to false in production

        'db' => [
        	'driver' => 'mysql',
        	'host' => 'localhost',
        	'database' => 'WebServiceEvent',
        	'username' => 'root',
        	'charset' => 'utf8',
        	'collation' => 'utf8_unicode_ci',
        	'prefix' => '',

    	]
        ],

	]);

$container = $app->getContainer(); 

$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container['settings']['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();


$container['db'] = function ($container) use ($capsule) {

	return $capsule;
};



$container['view'] = function ($container) {
	$view = new \Slim\Views\Twig (__DIR__ .'/../templates' , [
		'cache' => false, 
	]);

	$view->addExtension(new \Slim\Views\TwigExtension(
		$container->router,
		$container->request->getUri()
		));


	$view->getEnvironment()->addGlobal('daouser', [

		'check' => $container->daouser->check(),

		'user' => $container->daouser->user(),

		]);

	
	
	$view->getEnvironment()->addGlobal('flash', $container->flash);


	return $view;
};




$container['validator'] = function ($container) {

	return new WebServiceEvent\Validation\validator;
};


//Controllers
$container['AccueilController'] = function($container){
	return new \WebServiceEvent\Controllers\AccueilController($container);
};

$container['AuthController'] = function($container){
	return new \WebServiceEvent\Controllers\Authentication\AuthController($container);
};

$container['PasswordController'] = function($container){
	return new \WebServiceEvent\Controllers\Authentication\PasswordController($container);
};


$container['ControllerSoiree'] = function($container){
	return new \WebServiceEvent\Controllers\ControllerSoiree\ControllerSoiree($container);
};

$container['ContactController'] = function($container){
	return new \WebServiceEvent\Controllers\ContactController\ContactController($container);
};



//DAO 
$container['daosoiree'] = function ($container) {
	return new \WebServiceEvent\Dao\DAOSoiree;
};

$container['daouser'] = function ($container) {
	return new \WebServiceEvent\Dao\DAOUser;
};



$container['flash'] = function ($container) {
	return new \Slim\Flash\Messages;
};
$container['csrf'] = function ($container) {
	return new \Slim\Csrf\Guard;
};






$app->add(new \WebServiceEvent\Middleware\ValidationErrorsMiddleware($container));

$app->add(new \WebServiceEvent\Middleware\OldInputMiddleware($container));

$app->add(new \WebServiceEvent\Middleware\CsrfViewMiddleware($container));

$app->add($container->csrf);


v::with('WebServiceEvent\\Validation\\Rules\\');

require __DIR__ . '/../src/routes.php';