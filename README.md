<!--# Slim Framework 3 Skeleton Application

Use this skeleton application to quickly setup and start working on a new Slim Framework 3 application. This application uses the latest Slim 3 with the PHP-View template renderer. It also uses the Monolog logger.

This skeleton application was built for Composer. This makes setting up a new Slim Framework application quick and easy.

## Install the Application

Run this command from the directory in which you want to install your new Slim Framework application.

    php composer.phar create-project slim/slim-skeleton [my-app-name]

Replace `[my-app-name]` with the desired directory name for your new application. You'll want to:

* Point your virtual host document root to your new application's `public/` directory.
* Ensure `logs/` is web writeable.

To run the application in development, you can also run this command. 

	php composer.phar start

Run this command to run the test suite

	php composer.phar test

That's it! Now go build something cool.-->

# WebServiceEvent

Ce site à été créer à l'aide de SLIM Framework 3.
Un site web qui vous permet de consulter des soirées afin d'y participer, mais aussi d'ajouter vos propres soirées pour qu'il y est un maximum de monde.

## Comment l'utilisée?

En simple visiteur vous ne pourrez que consulter les soirées.

Pour plus d'option inscrivez-vous!

En tant qu'utilisateur vous pourrez:

	- vous connectez
	- ajouter une soirée
	- modifier ou suprimmer une soirée que si elle vous appartient
